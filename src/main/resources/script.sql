CREATE USER 'entmaster' IDENTIFIED BY '3ntm45t3r';

CREATE DATABASE ents CHARACTER SET utf8 COLLATE utf8_general_ci;

GRANT ALL ON ents.* TO 'entmaster';

CREATE TABLE copses (
    id INT NOT NULL AUTO_INCREMENT,
    surface VARCHAR(256),
    PRIMARY KEY (id)
);

CREATE TABLE ents (
    id INT NOT NULL AUTO_INCREMENT,
    copse INT,
    FOREIGN KEY (copse) REFERENCES copses (id),
    age INT,
    species VARCHAR(256),
    PRIMARY KEY (id)
);