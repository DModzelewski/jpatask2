package copses;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

public class CopseDao {

    public Copse persist(Copse copse) {
        EntityManager em = CopseEMF.createEM();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(copse);
        tx.commit();
        em.close();
        return copse;
    }

    public Copse merge(Copse copse){
        EntityManager em = CopseEMF.createEM();
        EntityTransaction et = em.getTransaction();
        et.begin();
        copse = em.merge(copse);
        et.commit();
        em.close();
        return copse;
    }

    public Copse find(int id){
        EntityManager em = CopseEMF.createEM();
        Copse copse = em.find(Copse.class, id);
        em.close();
        return copse;
    }

    public void delete(Copse copse){
        EntityManager em = CopseEMF.createEM();
        EntityTransaction et = em.getTransaction();
        et.begin();
        em.remove(em.merge(copse));
        et.commit();
        em.close();
    }

    public List<Copse> findAll(){
        EntityManager em = CopseEMF.createEM();

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Copse> query = cb.createQuery(Copse.class);
        query.from(Copse.class);
        List<Copse> copses = em.createQuery(query).getResultList();
        em.close();
        return copses;
    }

}
