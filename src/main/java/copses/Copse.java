package copses;

import javax.persistence.*;

@Entity
@Table(name = "copses")
public class Copse {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String surface;

    public Copse(String surface) {
        this.id = id;
        this.surface = surface;
    }

    public Copse() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSurface() {
        return surface;
    }

    public void setSurface(String surface) {
        this.surface = surface;
    }
}
