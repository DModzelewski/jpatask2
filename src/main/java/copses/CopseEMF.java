package copses;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class CopseEMF {
    private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("EntsPU");

    public static EntityManager createEM(){
        return emf.createEntityManager();
    }

    public static void close(){
        emf.close();
    }
}
